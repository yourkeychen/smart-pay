
CREATE TABLE [dbo].[t_refund_order](
	[RefundOrderId] [varchar](30) NOT NULL,
	[PayOrderId] [varchar](30) NOT NULL,
	[ChannelPayOrderNo] [varchar](64) NULL,
	[MchId] [varchar](30) NOT NULL,
	[MchRefundNo] [varchar](30) NOT NULL,
	[ChannelId] [varchar](24) NOT NULL,
	[PayAmount] [bigint] NOT NULL,
	[RefundAmount] [bigint] NOT NULL,
	[Currency] [varchar](3) NOT NULL,
	[Status] [int] NOT NULL,
	[Result] [int] NOT NULL,
	[ClientIp] [varchar](32) NULL,
	[Device] [varchar](64) NULL,
	[RemarkInfo] [varchar](256) NULL,
	[ChannelUser] [varchar](32) NULL,
	[UserName] [varchar](24) NULL,
	[ChannelMchId] [varchar](32) NOT NULL,
	[ChannelOrderNo] [varchar](32) NULL,
	[ChannelErrCode] [varchar](128) NULL,
	[ChannelErrMsg] [varchar](128) NULL,
	[Extra] [varchar](512) NULL,
	[NotifyUrl] [varchar](128) NOT NULL,
	[Param1] [varchar](64) NULL,
	[Param2] [varchar](64) NULL,
	[ExpireTime] [datetime] NULL,
	[RefundSuccTime] [datetime] NULL,
	[CreateTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
 CONSTRAINT [PK_t_refund_order] PRIMARY KEY CLUSTERED 
(
	[RefundOrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付单号 一个支付单号可以多次退款' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_refund_order', @level2type=N'COLUMN',@level2name=N'PayOrderId'
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ChannelPayOrderNo]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('cny') FOR [Currency]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('0') FOR [Status]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT ('0') FOR [Result]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ClientIp]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [Device]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [RemarkInfo]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ChannelUser]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [UserName]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ChannelOrderNo]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ChannelErrCode]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ChannelErrMsg]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [Extra]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [Param1]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [Param2]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [ExpireTime]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (NULL) FOR [RefundSuccTime]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO

ALTER TABLE [dbo].[t_refund_order] ADD  DEFAULT (getdate()) FOR [UpdateTime]
GO


